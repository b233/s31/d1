const User = require('./../models/User');

// module.exports.createUser
// const createUser = function(reqBody){
	// chaged to mode.exports
module.exports.createUser = function(reqBody){
	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		userName: reqBody.userName,
		password: reqBody.password
	});

	return newUser.save().then( (savedUser, error)=>{
		if(error){
			return error
		} else {
			return savedUser
		}
	});
}

module.exports.getAll = () => {
	return User.find().then ((result, error)=>{
		if(error) {
			return error
		} else {
			return result
		}
	})
}

module.exports.getUser = (params) => {
	return User.findById(params).then ((result)=> {
		return result
	})
};

module.exports.updateUser = (params, reqBody) => {
	return User.findByIdAndUpdate(params, reqBody, {new: true}).then ((result, error)=> {
		if(error) {
			return error
		} else {
			return result
		}
	})
};

module.exports.delUser = (params) => {
	return User.findByIdAndDelete(params).then ((result, error)=> {
		if(error) {
			return error
		} else {
			return true
		}
	})
};