const express = require ('express');
const mongoose = require('mongoose');
const app = express();
const PORT = 5000;

const tasksRoutes = require('./routes/tasksRoutes');

app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.connect("mongodb+srv://chardrichdev:Hayahay2023Chadiks@cluster0.irgko.mongodb.net/s31?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true,
	}
// ).then(()=> {console.log(`Connected to the DB`)});
// .catch((error)=> { console.log(error)});

).then(()=> { //if the mongoose suceeded on the connection, then we will console.log message
	console.log("Successfully Connected to Database!");
}).catch((error)=> { //handles error when the mongoose failed to connect on our mongodb atlas database
	console.log(error);
});
app.use("/", tasksRoutes);

app.listen(PORT, ()=> console.log (`Server is running on ${PORT}`));

