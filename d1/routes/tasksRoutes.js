const express = require ('express');
const router = express.Router();
const User = require('./../models/User');

const userController = require('./../controllers/userControllers');
// router.post('/addTask', (req, res)=>{
// 	let requestBodyData = req.body
// 	res.send(requestBodyData);
// });

// router.get('/tasks', (req, res)=>{
// 	res.send(`this is coming from the task route`)
// });

router.post('/add-task', (req, res)=>{
	// console.log(req.body);
	// let newUser = new User ({
	// 				firstName: req.body.firstName,
	// 				lastName: req.body.lastName,
	// 				userName: req.body.userName,
	// 				password: req.body.password
	// 			})
	// newUser.save((savedUser, error)=>{
	// 	if(error){
	// 		res.send(error)
	// 	} else {
	// 		res.send(`New user saved:`, savedUser);
	// 	}
	// })
	userController.createUser(req.body).then(result=>res.send(result));
	// userController.createUser(req.body).then((result)=>{
	// 	res.send(result)
	// })
});

router.get("/users", (req, res)=> {
	// User.find({}, (result, error)=>{
	// 	if(error){
	// 		res.send(error)
	// 	} else {
	// 		res.send(`Users:`, result);
	// 	}
	// })
	userController.getAll().then(result=>res.send(result));
});


router.get("/users/:id", (req, res)=> {
	// console.log(req.params);
	// let params = req.params.id
	// User.findById(params, (result, error)=>{
	// 	if (error){
	// 		res.send(error)
	// 	} else {
	// 		res.send(result);
	// 	}
	// })
	let params = req.params.id
	userController.getUser(params).then(result=>res.send(result));
});

router.put("/users/:id", (req, res)=> {
	let params = req.params.id
	userController.updateUser(params, req.body).then(result=>res.send(result));
	// User.findByIdAndUpdate(params, req.body, {new: true}, (result, error)=> {
	// 	if(error){
	// 		res.send(error);
	// 	} else {
	// 		res.send(result);
	// 	}
	// })
});

router.delete("/users/:id", (req, res)=>{
	// User.findByIdAndDelete(params, (error, result)=>{
	// 	if(error){
	// 		res.send(error)
	// 	} else {
	// 		res.send(true);
	// 	}
	// })
	let params = req.params.id
	userController.delUser(params).then(result=>res.send(result));
});

module.exports = router;